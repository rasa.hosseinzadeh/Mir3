import numpy
from elasticsearch import Elasticsearch
from elasticsearch_dsl.connections import connections
from numpy import linalg, matrix, matmul, multiply, add


id_to_index = {}
index_to_id =  {}
def get_indexed_docs_in_elasticsearch():
    client = Elasticsearch()
    #connections.create_connection(hosts=["localhost"], port=9200)

    number_of_indexed_docs = client.count()['count']
    docs = client.search(size=number_of_indexed_docs,index='wiki',doc_type='json')['hits']['hits']

    index=0
    for doc in docs:
        id_to_index[doc['_id']] = index
        index_to_id[index] = doc['_id']
        index += 1

    for i in range(len(docs)) :
        if(i>=len(docs)):
            break
        doc =docs[i]
        try:(x,y)= (doc['_source']['url'] , doc['_id'])
        except:
            del docs[i]

    return docs


def normalize_matrix(p_matrix, alpha):
    for row in range(len(p_matrix)):
        sum_of_row = 0
        for col in range(len(p_matrix)):
            sum_of_row += p_matrix[row][col]
        for col in range(len(p_matrix)):
            if sum_of_row > 0:
                p_matrix[row][col] /= sum_of_row
            else:
                p_matrix[row][col] = (1 / len(p_matrix))

    p_matrix = matrix(p_matrix)
    p_matrix = multiply(p_matrix, 1 - alpha)
    p_matrix = add(p_matrix, alpha / len(p_matrix))
    return p_matrix


def create_p_matrix(alpha):
    docs = get_indexed_docs_in_elasticsearch()
    url_to_id = {}
    for doc in docs:
        url_to_id[doc['_source']['url']] = doc['_id']

    p_matrix = [[0 for x in range(len(docs))] for y in range(len(docs))]

    for doc in docs:
        row = id_to_index[doc['_id']]
        for out_url in doc['_source']['out_urls']:
            if url_to_id.get(out_url) is not None:
                column = id_to_index[url_to_id.get(out_url)]
                p_matrix[row - 1][column - 1] = 1

    p_matrix = normalize_matrix(p_matrix, alpha)
    return p_matrix


def calculate_page_rank(alpha):
    p_matrix = create_p_matrix(alpha)
    rank_matrix = [0 for x in range(len(p_matrix))]
    rank_matrix[0] = 1
    for i in range(1000):
        rank_matrix = matmul(matrix(rank_matrix), p_matrix)
    return rank_matrix


def index_page_rank(alpha):
    client = Elasticsearch()
    rank_matrix = calculate_page_rank(alpha)
    docs = get_indexed_docs_in_elasticsearch()

    for doc in docs:
        page_rank = rank_matrix.item(id_to_index[doc['_id']])
        client.update(index="wiki", doc_type='json', id=int(doc['_id']), body={'doc': {'page_rank':page_rank }})
