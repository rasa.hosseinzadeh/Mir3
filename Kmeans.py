import json
import re
from random import sample

import sys
from elasticsearch import Elasticsearch
from numpy.ma import sqrt, log

import math



def get_term_vec(id:int): #check if doc does not exist
    es = Elasticsearch()
    es.indices.open(index="wiki", ignore=404)
    v=es.termvectors(index="wiki", doc_type="json", id=id,fields=['content'])
    v=v['term_vectors']['content']['terms']

    term_vec={}
    length=0;
    for term in v:
        term_vec[term]= v[term]['term_freq']
        length += term_vec[term]**2
    length = sqrt(length)
    for term in v:
        term_vec[term] /= length
    return term_vec

def add_vec(vec1,vec2):
    res = {}
    for term in vec1:
        c1 = vec1[term]
        c2 = vec2.get(term,0)
        res[term]=c1+c2

    for term in vec2:
        if(term not in vec1):
            res[term]=vec2[term]

    return res


def distance(vec1,vec2,term_idf=None):
    sum=0
    for term in vec1:
        c1 = vec1[term]
        c2 = vec2.get(term,0)
        if(term_idf is None):
            idf = 1
        else:
            idf = term_idf[term]
        sum += (c1-c2)**2
    for term in vec2:
        if(term not in vec1):
            sum += vec2[term]**2

    return sum

def cosineSim(vec1,vec2,term_idf=None):
    sim=0
    for term in vec1:
        c1 = vec1[term]
        c2 = vec2.get(term,0)
        if(term_idf is None):
            idf = 1
        else:
            idf = term_idf[term]
        sim += idf*(c1*c2)
    return sim

def k_means(cluster_limit:int, iteration_count:int, max_id:int, acceptance_cost_fraction:float):

    termVectors = {}
    terms = {}
    valid_doc_count=0
    for i in range(1,max_id+1):
        try:
            termVectors[i] = get_term_vec(i)
            valid_doc_count += 1
            for term in termVectors[i]:
                if(terms.get(term) is None):terms[term] = 0
                terms[term] += 1

        except:
            pass

    for term in terms:
        terms[term] = math.log10(valid_doc_count/terms[term])
    if(cluster_limit==-1):cluster_limit=valid_doc_count

    cost = (cluster_limit+1)*[None]

    (cluster_centers, doc_to_clusters) = (None, None)
    for clusterCount in range(1,cluster_limit+1):
        print("Now running with {} cluster{}".format(clusterCount,"s" if clusterCount>1  else ""))
        (prev_clusters,prev_doc_to_clusters) = (cluster_centers, doc_to_clusters)
        (cluster_centers, doc_to_clusters,cost[clusterCount]) = kmeans_with_clustr_count(clusterCount,iteration_count,termVectors,terms)
        print(cost[clusterCount])
        if(clusterCount>1 and cost[clusterCount]>acceptance_cost_fraction*cost[clusterCount-1]):
            (cluster_centers, doc_to_clusters, cost[clusterCount]) = kmeans_with_clustr_count(clusterCount,
                                                                                               iteration_count,
                                                                                               termVectors, terms)
            print(cost[clusterCount])
            if (clusterCount > 1 and cost[clusterCount] > acceptance_cost_fraction * cost[clusterCount - 1]):
                labels =name_clusters(prev_doc_to_clusters,clusterCount-1,termVectors,terms)
                return (prev_clusters,prev_doc_to_clusters,labels)

    labels = name_clusters(doc_to_clusters, clusterCount , termVectors, terms)
    return (cluster_centers, doc_to_clusters,labels)



def cluster_cost(cluster_centers:dict, doc_to_clusters:dict, term_vectors:dict):
    cost = 0

    for doc in doc_to_clusters:
        cost += distance(cluster_centers[doc_to_clusters[doc]],term_vectors[doc])

    return cost

def kmeans_with_clustr_count(clusterCount:int, iteration_count:int, termVectors:dict, terms:set):
    doc_to_clusters={}
    clusterCenters= (clusterCount + 1) * [{}]
    clusterDocCount= (clusterCount + 1) * [0]
    prevCost = math.inf

    #initializing centroids
    center_docs = sample(termVectors.keys(), clusterCount)
    for i in range(1, clusterCount+1):
        clusterCenters[i]=termVectors[center_docs[i - 1]].copy()



    for _ in range(0,iteration_count):
        isChanged = False
        totalCost = 0
        totalSim=0
        for i in range(1, clusterCount+1):
            clusterDocCount[i] = 0


        #doc assignment phase
        for doc in termVectors:
            currentTermVector = termVectors[doc]
            maxSim = -math.inf
            bestCluster=0
            for i in range(1, clusterCount+1):
                sim = cosineSim(currentTermVector,clusterCenters[i],term_idf=terms)
                if(sim>maxSim):
                    maxSim=sim
                    minDist = distance(currentTermVector, clusterCenters[i], term_idf=terms)
                    bestCluster=i

            if(doc_to_clusters.get(doc)!=bestCluster):
                isChanged=True

            totalCost +=  minDist
            totalSim += maxSim
            doc_to_clusters[doc] = bestCluster
            clusterDocCount[bestCluster] += 1

        #center calculation phase
        for i in range(1, clusterCount + 1):
            clusterCenters[i] = {}

        for doc in termVectors:
            clusterCenters[doc_to_clusters[doc]] = add_vec(clusterCenters[doc_to_clusters[doc]], termVectors[doc])

        for i in range(1, clusterCount+1):
            count = clusterDocCount[i]
            if(count == 0):
                print("Cluster without node",file=sys.stderr)
            else:
                for term in clusterCenters[i]:
                    clusterCenters[i][term] /= count

        if(not isChanged or totalCost>.99*prevCost):break
        prevCost = totalCost

    return (clusterCenters,doc_to_clusters,totalCost)


def name_clusters(doc_to_cluster, clusterCount, termVectors, terms):
    Pt = {}
    termInCluster = (clusterCount+1)*[None]
    Hc = (clusterCount+1)*[0]
    Ht = {}
    Pc=(clusterCount+1)*[0]

    #counting
    for doc in doc_to_cluster:
        cluster = doc_to_cluster[doc]
        Pc[cluster] += 1
        if (termInCluster[cluster] is None):termInCluster[cluster]={}
        for term in termVectors[doc]:
            if(termInCluster[cluster].get(term) is None):termInCluster[cluster][term]=0
            termInCluster[cluster][term] += 1
            if (Pt.get(term) is None): Pt[term] = 0
            Pt[term] += 1


    #normalizing
    Pct = [{} for _ in range(1+clusterCount)]
    s = sum(Pt.values())
    for cluster in range(1,clusterCount+1):
        for term in terms:
            Pct[cluster][term]={}
            Pct[cluster][term][(1, 1)] = termInCluster[cluster].get(term,0)/s
            Pct[cluster][term][(1, 0)] = (Pc[cluster] - termInCluster[cluster].get(term,0))/s
            Pct[cluster][term][(0, 1)] = (Pt[term] - termInCluster[cluster].get(term,0))/s
            Pct[cluster][term][(0, 0)] = 1-(Pct[cluster][term][(1, 1)]+Pct[cluster][term][(1, 0)]+Pct[cluster][term][(0, 1)])

    #s = sum(Pt.values())
    for term in terms:
        Pt[term] /= s
        Pt[term] = [1-Pt[term],Pt[term]]
        try:
            Ht[term] = -(Pt[term][0]*math.log10(Pt[term][0])+Pt[term][1]*math.log10(Pt[term][1]))
        except:
            Ht[term] = 0

    s = sum(Pc)
    for i in range(1,clusterCount+1):
        Pc[i] /=s
        Pc[i] = [1 - Pc[i], Pc[i]]
        try:
            Hc[i] = -(Pc[i][0] * math.log10(Pc[i][0]) + Pc[i][1] * math.log10(Pc[i][1]))
        except:
            Hc[i] = 0

    mutual_inf = (clusterCount+1)*[None]

    for cluster in range(1,clusterCount+1):

        mutual_inf[cluster]={}
        for term in terms:
            mutual_inf[cluster][term]=0
            for c in [0, 1]:
                for t in [0, 1]:
                    if(Pc[cluster][c]*Pt[term][t]*Pct[cluster][term][(c,t)] != 0):
                       mutual_inf[cluster][term] += Pct[cluster][term][(c,t)] * math.log10(Pct[cluster][term][(c,t)]/(Pc[cluster][c]*Pt[term][t]))

            try:
                mutual_inf[cluster][term] /= (Hc[cluster] + Ht[term])
            except:
                mutual_inf[cluster][term] = 0

    informativeWords = [ [ (0, None) for _ in range(0,5)  ] for _ in range(0,clusterCount+1)]
    for cluster in range(1, clusterCount + 1):
        for term in terms:
            if (mutual_inf[cluster][term],term) > informativeWords[cluster][0]:
                informativeWords[cluster][0] = (mutual_inf[cluster][term],term)
                informativeWords[cluster].sort()

    for cluster in range(1, clusterCount + 1):
        informativeWords[cluster] = [x for (_,x) in informativeWords[cluster]]



    return informativeWords


def add_cluster_to_index(doc_to_cluster,labels):
    client = Elasticsearch()
    client.indices.open(index="wiki", ignore=404)
    for i in doc_to_cluster:
        client.update(index='wiki',doc_type="json", id=i, body={'doc':{'cluster_id':doc_to_cluster[i],'cluster_name':labels[doc_to_cluster[i]]}})