# -*- coding: utf-8 -*-
import re
import threading
from random import shuffle

import scrapy
from bs4 import BeautifulSoup
from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings

START_CALLER = "start_url"

class WikiSpider(scrapy.Spider):
    DEFAULT_REQUEST_COUNT_LIMIT = 1000
    DEFUALT_OUT_DEGREE = 10
    DEFUALT_START_URL = "https://fa.wikipedia.org/wiki/%D8%B3%D8%B9%D8%AF%DB%8C"
    name = "wiki"
    allowed_domains = ["fa.wikipedia.org"]

    def __init__(self,
                 request_count_limit=DEFAULT_REQUEST_COUNT_LIMIT,
                 out_degree=DEFUALT_OUT_DEGREE,
                 start_urls=[DEFUALT_START_URL]):

        self.caller = {}
        self.lock = threading.Lock()
        self.custom_settings = {
            'ROBOTSTXT_OBEY': True,
            'DEPTH_PRIORITY': 1000,
            'SCHEDULER_DISK_QUEUE': 'scrapy.squeues.PickleFifoDiskQueue',
            'SCHEDULER_MEMORY_QUEUE': 'scrapy.squeues.FifoMemoryQueue',
            'CLOSESPIDER_PAGECOUNT': request_count_limit,
            'CLOSESPIDER_ITEMCOUNT': request_count_limit,
            'ITEM_PIPELINES': {'Crawler.pipelines.CrawlerPipeline': 300},
            'BOT_NAME': 'Crawler',
            'NEWSPIDER_MODULE': 'Crawler.spiders',
            'CONCURRENT_REQUESTS_PER_DOMAIN': 1,
            'LOG_LEVEL': 'ERROR',
        }

        self.seen_url = []
        self.out_degree = out_degree
        self.request_count_limit = request_count_limit
        self.request_count = len(start_urls)  # becuase start_urls don't have the increment in parse
        self.start_urls = start_urls
        self.seen_url.append(self.start_urls)

    def hasNumCol(self, title):
        return bool(re.search(r'[0-9۰-۹::]', title))

    def isRelOrFileAdr(self, href):
        return '.' in href.split(sep='/')[-1] or '#' in href
        return

    def parse(self, response):

        self.lock.acquire()
        valid_urls = []
        for link in response.css('#mw-content-text a'):
            title = link.css(r"::attr(title)").extract_first()
            isRed = (link.css(r"::attr(class)").extract_first() == "new")
            href = link.css(r"::attr(href)").extract_first()
            if (isRed or title is None or self.hasNumCol(title) or href is None or self.isRelOrFileAdr(href)):
                continue
            href = response.urljoin(href)

            if ("fa.wikipedia.org" in href):
                valid_urls.append(href)

        shuffle(valid_urls)

        title = response.xpath('//*[(@id = "firstHeading")]//text()').extract_first()
        if (title is None): title = "No Title"

        summary = (response.xpath('//div[@id = "mw-content-text"]/p[./b][1]').extract_first())
        if (summary is None or summary.isspace() or summary == ""):
            summary = ' '.join(
            response.xpath('//*[@id="mw-content-text"]//p[1]//text()').extract())

        else:
            summary = BeautifulSoup(summary).text

        infobox_keys = response.xpath(r'//*[contains( @class ,"infobox")]//tr[count(./*)>1]/*[1]').extract()
        infobox_values = response.xpath(r'//*[contains( @class ,"infobox")]//tr[count(./*)>1]/*[2]').extract()
        infobox = {}
        if (infobox_keys is not None and infobox_values is not None):
            for i in range(len(infobox_keys)):
                key = infobox_keys[i]
                value = infobox_values[i]
                if (key is None or value is None): break
                key = BeautifulSoup(key).text
                value = BeautifulSoup(value).text
                infobox[key] = value

        content_xpath_query = """
            //h2[contains(./.,"پانویس") 	or
            contains(./.,"مراجع") 			or
            contains(./.,"منابع") 			or
            contains(./.,"پیوند به بیرون") 	or
            contains(./.,"مطالعه بیشتر") 	or
            contains(./.,"جستارهای وابسته") or
            contains(./.,"نگارخانه")       or
            contains(./.,"پی‌نوشت")]
            [1]/preceding-sibling::*[
            (descendant-or-self::p |
            descendant-or-self::h2 |
            descendant-or-self::h3 |
            descendant-or-self::h4 |
            descendant-or-self::h5 |
            descendant-or-self::ol |
            descendant-or-self::ul |
            descendant-or-self::tr |
            descendant-or-self::dl |
            descendant-or-self::th) and
            (not(contains(@class,"box") or contains(@class,"toc"))) ]
        """
        content = ' '.join(response.xpath(content_xpath_query + "/.").extract())
        content = BeautifulSoup(content).text
        content = re.sub(r'\[.*?\]', ' ', content)
        content = re.sub(r' +', ' ', content)
        content = re.sub(r'[\n\r]\s*[\r\n]*', '\n', content)
        if (content.isspace() or content == ""):
            content = ' '.join(response.xpath(content_xpath_query + "/text()").extract())
            content = re.sub(r'\[.*?\]', ' ', content)
            content = re.sub(r' +', ' ', content)
            content = re.sub(r'[\n\r]\s*[\r\n]*', '\n', content)


        yield {
            'title': title,
            'url': response.url,
            'caller': self.caller.get(response.url, START_CALLER),
            'summary': re.sub("\[.*?\]"," ",summary),
            'infobox': infobox,
            'content': content,
            'out_urls': valid_urls,
        }

        out_count = 0
        for url in valid_urls :
            if (out_count > self.out_degree): break
            if (url in self.seen_url): continue
            self.seen_url.append(url)
            out_count = out_count + 1
            if (self.request_count < self.request_count_limit):
                self.request_count = self.request_count + 1
                self.caller[url] = response.url
                yield scrapy.Request(url, callback=self.parse)
        self.lock.release()

    def start_crawling(self):
        settings = Settings()

        for option in self.custom_settings:
            settings.set(option, self.custom_settings[option])
        process = CrawlerProcess(settings=settings)
        process.crawl(crawler_or_spidercls=self,
                      request_count_limit=self.request_count_limit,
                      out_degree=self.out_degree,
                      start_urls=self.start_urls
                      )
        process.start()

