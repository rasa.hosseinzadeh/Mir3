# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import codecs
import json
import os
import shutil

from tqdm import tqdm

SAVING_DIRECTORY = 'wiki/'

class CrawlerPipeline(object):
    count = 0
    toolbar_width = 40
    current_width = 0;

    def open_spider(self, spider):
        try:
            shutil.rmtree(SAVING_DIRECTORY)
        except:
            pass
        os.makedirs(os.path.dirname(SAVING_DIRECTORY), exist_ok=True)
        self.bar = tqdm(total=spider.request_count_limit)


    def process_item(self, item, spider):
        self.count = self.count + 1
        with codecs.open(SAVING_DIRECTORY + str(self.count) + "-" + item['title'] + ".json", 'w',
                         encoding='utf-8') as file:
            line = json.dumps(item, ensure_ascii=False)
            file.write(line)
        self.bar.update(1)
        return item

    def close_spider(self, spider):
        self.bar.close()
