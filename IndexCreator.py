import os
import json

import time
import traceback

from elasticsearch import Elasticsearch
from elasticsearch_dsl import DocType
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import token_filter,char_filter
from Crawler.pipelines import SAVING_DIRECTORY
from elasticsearch_dsl import Text,Object,analyzer,Index,Keyword

#static variables
zero_space=char_filter('zero_width_space',type="mapping",mappings=[ "\\u200C=> "])
persian_stop = token_filter('persian_stop',type="stop",stopwords="_persian_")

number_stop = token_filter('number_stop', type="pattern_replace",
                           pattern=".[1]|(.*[۰-۹\\d]+(.*[۰-۹\\d]+)?)|[^ا-ی]",
                           replacement="")
persian_analyzer = analyzer('persian_analyzer',
tokenizer = 'standard',
filter = ["lowercase", "arabic_normalization", "persian_normalization", persian_stop,number_stop],
char_filter = [zero_space]
)

class WikiDoc(DocType):
    title = Keyword()
    url = Text(analyzer = "whitespace")
    caller = Text(analyzer = "whitespace")
    summary = Text(analyzer = persian_analyzer)
    content = Text(analyzer = persian_analyzer)
    out_urls = Text(multi=True,analyzer = "whitespace")



    class Meta:
        index = 'wiki'
        doc_type = "json"

    def save(self, ** kwargs):
        return super().save(** kwargs)

    def to_dict(self):
        return {
            'title':str(self.title),
            'url': str(self.url),
            'caller':str(self.caller),
            'summary':str(self.summary),
            'content':str(self.content),
            'out_urls':self.out_urls,
        }


def add_docs_to_elastic():
    #client = ES.Elasticsearch()
    connections.create_connection(hosts=["localhost"],port=9200)

    wikiIndex = Index('wiki')
    wikiIndex.delete(ignore=404)
    wikiIndex.create()
    wikiIndex.doc_type(WikiDoc)
    wikiIndex.close()
    WikiDoc.init()
    wikiIndex.open()

    for fileName in os.listdir(SAVING_DIRECTORY):
        if fileName.endswith('.json'):
            with open(SAVING_DIRECTORY + fileName) as open_file:
                print(fileName)
                data = json.load(open_file)
                instance = WikiDoc(title = data['title'],
                        url = data['url'],
                        caller = data['caller'],
                        summary = data['summary'],
                        content = data['content'],
                        out_urls = data['out_urls'],
                        )
                instance.meta.id = int(fileName.split("-")[0])
                instance.save()
                '''try:
                    instance.save()
                except  Exception:
                    pass#print(traceback.format_exc())'''


def delete_index():
    client = Elasticsearch()
    client.indices.delete(index="wiki", ignore=404)
'''
def add_docs_to_elastic():
    client = Elasticsearch()
    connections.create_connection(hosts=["localhost"],port=9200)
    client.indices.delete(index="wiki",ignore=404)
    client.indices.create(index="wiki", ignore=404)

    for fileName in os.listdir(SAVING_DIRECTORY):
        with open(SAVING_DIRECTORY+fileName) as file:
            data = json.load(file)
            print(fileName)
            try:
                client.index(index="wiki", doc_type='json', id=fileName.split("-")[0], body=data)
            except:
                pass
    client.indices.close(index="wiki",ignore=404)

'''