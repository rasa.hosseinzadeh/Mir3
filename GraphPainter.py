import json
import os
import subprocess
from os import listdir
from os.path import isfile, join
import graphviz as gv

import Crawler.spiders.WikiSpider as WikiSpider


def draw():
    dir = "wiki/"
    url_to_title = {}
    edges = set()
    caller_edges = set()
    start_nodes=set()
    fileNames = [dir + f for f in listdir(dir) if str(f).endswith(".json") and isfile(join(dir, f))]
    for fileName in fileNames:
        with open(fileName, mode='r') as file:
            data = json.load(file)
            url_to_title[data['url']] = data['title']

    for fileName in fileNames:
        with open(fileName, mode='r') as file:
            data = json.load(file)
            if (data['caller'] != WikiSpider.START_CALLER):
                try:
                    caller_edges.add((url_to_title[data['caller']], data['title']))
                except:
                    pass
            else:
                start_nodes.add(data['title'])

            for out_url in data['out_urls']:
                if (url_to_title.get(out_url) is not None):
                    try:
                        edges.add((data['title'], url_to_title[out_url]))
                    except:
                        pass
    graph = gv.Digraph(format="png")

    graph.graph_attr.update()
    for url in url_to_title:
        title = url_to_title[url]
        if(title in start_nodes):
            graph.node(title,shape="hexagon",fillcolor="#42D0FF",fontcolor="white", style="filled")
        else:
            graph.node(title,fillcolor="white",style="filled")

    for (source,dest) in edges:
        if((source,dest) in  caller_edges):
            graph.edge(source,dest,color='red')
        #else:
            #graph.edge(source, dest)

    temp = open('crawlerGraph', 'w')
    temp.write(graph.source)
    temp.close()
    subprocess.call('/bin/bash -c "$GREPDB"', shell=True, env = {'GREPDB': 'dot -T svg -O  crawlerGraph'})
    os.remove("crawlerGraph")
