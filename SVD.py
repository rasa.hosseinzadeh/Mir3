import math

import numpy
from elasticsearch import Elasticsearch
from numpy.linalg import svd
from numpy.ma import sqrt
from numpy import matrix

def get_term_vec(id:int): #check if doc does not exist
    es = Elasticsearch()
    es.indices.open(index="wiki", ignore=404)
    v=es.termvectors(index="wiki", doc_type="json", id=id,fields=['content'])
    try:v=v['term_vectors']['content']['terms']
    except: v={}

    term_vec={}
    length=0;
    for term in v:
        term_vec[term]= 1+math.log10(v[term]['term_freq'])
        length += term_vec[term]**2
    length = sqrt(length)
    for term in v:
        term_vec[term] /= length
    return term_vec


def computeSVD(maxId:int):
    termVectors = {}
    term_to_index = {}
    term_count=0
    for i in range(1,maxId+1):
        termVectors[i] = get_term_vec(i)
        for term in termVectors[i]:
            if(term_to_index.get(term) is None):
                term_to_index[term] =term_count
                term_count += 1
        #except:
            #print("faild to get tv for {}".format(i))

    temp = numpy.zeros((term_count,maxId))
    for doc in termVectors:
        for term in termVectors[doc]:
            temp[term_to_index[term]][doc-1]=termVectors[doc][term]

    m =  matrix(temp, dtype=numpy.float_, copy=True)

    (U,S,V)= svd(m,full_matrices=False,compute_uv=True)

    x = []
    y=[]
    with open("SVD.txt", "w") as f:
        for i in range(0,maxId):
            f.write(str((V[0,i,],V[1,i])))
            f.write("\n")
            x.append(V[0,i])
            y.append(V[1,i])
