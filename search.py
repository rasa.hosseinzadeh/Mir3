from elasticsearch import Elasticsearch
from elasticsearch_dsl import Q
from elasticsearch_dsl import Search


def search(query,cluster=0, pageRank=False):
    client = Elasticsearch()
    fields = ["title^10","summary^2","content^1"]

    s = Search(using=client, index='wiki',doc_type='json')

    if cluster > 0 :
        s = s.filter("match", cluster_id=cluster).query("multi_match", query=query, fields=fields)
    else:
        s = s.query("multi_match", query=query, fields=fields)
    if pageRank:
        s = s.sort('page_rank')

    response = s.execute()

    results = response.hits.hits
    for result in results:
        id = result['_id']
        source = result['_source']
        print("title = {}\nurl={}\nid={}\nSummary={}".format(source['title'],source['url'],id,source['summary']))
        print("=======================================================================")
