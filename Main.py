import json
import os
from os import system

from elasticsearch import Elasticsearch

import GraphPainter
from Crawler.pipelines import SAVING_DIRECTORY
from Crawler.spiders.WikiSpider import WikiSpider
from IndexCreator import add_docs_to_elastic,delete_index
from Kmeans import k_means, add_cluster_to_index
from PageRank import index_page_rank
from search import search
from SVD import computeSVD


def ui():
    isPageRankCalculated = False
    labels=[]
    k_means_acceptance_rate = 0.99
    k_means_iteration = 10


    while(True):
        print("Choices:\n1.Crawler\n2.Add to elastic\n3.Cluster\n4.Page Rank\n5.Search\n6.SVD\n7.exit\n")
        command = input()
        if command == "1":
            doc_count = int(input("Doc count\n"))
            out_degree = int(input("Out degree\n"))
            print("Input Start urls\nEnds with N as input")
            start_urls = []
            url = input("url/N\n")
            while(url!="N"):
                start_urls.append(url)
                url = input("url/N\n")
            draw = input("Would you like to draw the crawler graph?(Y/N)\n")
            spider = WikiSpider(request_count_limit=doc_count, out_degree=out_degree,start_urls=start_urls)
            spider.start_crawling()
            if(draw=="Y"):GraphPainter.draw()
            print("Crawler ended and saved crawled pages in wiki/ directory")
        elif command== "2":
            print("Choices:\n1.Add jsons to Elasticsearch\n2.Drop index")
            command = input()
            if(command=="1"):
                add_docs_to_elastic()
            elif(command=="2"):
                delete_index()
            else:print("Not valid input\n")
        elif command == "3":
            k_menas_cluster_limit = int(input("input an integer as k_means cluster limit(less than doc count)\n"))
            client = Elasticsearch()
            (_, doc_to_cluster, labels) = k_means(k_menas_cluster_limit, k_means_iteration, client.count()['count'], k_means_acceptance_rate)
            add_cluster_to_index(doc_to_cluster, labels)
        elif command == "4":
            alpha = float(input("Alpha for page rank(Floating point number in range(0,1)\n"))
            index_page_rank(alpha)
            isPageRankCalculated = True
        elif command == "5":
            cluster = 0
            if(labels):
                for i in range(1,len(labels)):
                    print(i,labels[i])
                cluster = int(input("Select one of the above clusters (0 for all)\n"))
            pageRank = False
            if(isPageRankCalculated):
                wantsPageRank = input("Sort results based on page rank(Y/N)\n")=="Y"
            else:
                wantsPageRank = input("Sort results based on page rank(if calculated on )(Y/N)\n") == "Y"
                if(wantsPageRank=="Y"):
                    pageRank = True
            query = input("Query String in one line\n")
            search(query,pageRank=pageRank,cluster=cluster)
        elif command == "6":
            computeSVD(client.count()['count'])
        elif command== "7":
            break
        else:
            print("not valid input")

ui()